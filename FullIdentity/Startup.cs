using FullIdentity.CustomValidate;
using FullIdentity.Models;
using FullIdentity.Requirements;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FullIdentity
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<AppIdentityDbContext>();

            services.AddTransient<IAuthorizationHandler, ExpireDateExchangeHandler>();

            services.AddAuthorization(conf => 
            {
                conf.AddPolicy("BakuPolicy",policy => 
                {
                    policy.RequireClaim("city","Baku"); //burda deyirikki citysi Baku olanlar ancaq Baku sehifesine gire bilsin elavede qeyd ede bilerik isdesek
                    
                });
                conf.AddPolicy("ViolencePolicy",policy => 
                {
                    policy.RequireClaim("birthdate");
                });

                conf.AddPolicy("ExchangePolicy",policy => 
                {
                    policy.AddRequirements(new ExpireDateExchangeRequirement());
                });
            });

            services.AddAuthentication().AddFacebook(opt =>
            {
                opt.AppId = Configuration["Authentication:Facebook:AppId"];
                opt.AppSecret = Configuration["Authentication:Facebook:AppSecret"];
            }).AddGoogle(opt => 
            {
                opt.ClientId = Configuration["Authentication:Google:ClientId"];
                opt.ClientSecret = Configuration["Authentication:Google:ClientSecret"];
            });
          
            services.AddIdentity<AppUser, AppRole>(opt =>
            {
                opt.User.RequireUniqueEmail = true;

                opt.Password.RequireUppercase = true;
                opt.Password.RequireNonAlphanumeric = false;
            }).AddPasswordValidator<CustomPasswordValidators>()
            .AddUserValidator<CustomUserValidator>()
            .AddErrorDescriber<CustomIdentityErrorDescriber>()
            .AddEntityFrameworkStores<AppIdentityDbContext>()
            .AddDefaultTokenProviders();

            services.ConfigureApplicationCookie(opt =>
            {
                opt.LoginPath = new PathString("/Home/Login");
                opt.LogoutPath = new PathString("/Member/LogOut");
                opt.Cookie.Name = "MyBlog"; //cookie  adini bele saxliyir
                opt.Cookie.HttpOnly = false;
                opt.Cookie.SameSite = SameSiteMode.Lax;//lax verdikde eyni saytdan olmadidqda bele request cookie elave eder strict olsa anca eyni saytdan qebul eder
                opt.Cookie.SecurePolicy = CookieSecurePolicy.SameAsRequest;
                opt.ExpireTimeSpan = TimeSpan.FromHours(1);
                opt.AccessDeniedPath = new PathString("/Home/AccessDenied");
            });

            services.AddScoped<IClaimsTransformation, ClaimProvider.ClaimProvider>();
            
            services.AddControllersWithViews();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();
            
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
