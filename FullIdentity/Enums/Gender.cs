﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FullIdentity.Enums
{
    public enum Gender
    {
        MelumDeyil = 0,
        Kisi = 1,
        Qadin = 2
    }
}
