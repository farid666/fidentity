﻿using FullIdentity.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FullIdentity.CustomTagHelpers
{
    [HtmlTargetElement("td",Attributes = "user-roles")]
    public class UserRoleTagHelper:TagHelper
    {
        public UserManager<AppUser> _userManager;

        [HtmlAttributeName("user-roles")]
        public string UserId { get; set; }


        public UserRoleTagHelper(UserManager<AppUser> userManager)
        {
            _userManager = userManager;
        }

        public async override Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
        {
            AppUser user = await _userManager.FindByIdAsync(UserId);

            IList<string> userRoles = await _userManager.GetRolesAsync(user);

            string html = null;

            userRoles.ToList().ForEach(x =>
            {
                html += $"<span class='badge' > {x} </span>";
            });

            output.Content.SetHtmlContent(html);
        }

    }
}
