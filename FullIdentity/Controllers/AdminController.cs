﻿using FullIdentity.Models;
using Mapster;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FullIdentity.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : BaseController
    {

        public AdminController(UserManager<AppUser> userManager, SignInManager<AppUser> signInManager, RoleManager<AppRole> roleManager) : base(userManager, signInManager, roleManager)
        {

        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Users()
        {
            return View(_userManager.Users.ToList());
        }

        public IActionResult Roles()
        {
            return View(_roleManager.Roles.ToList());
        }

        [HttpGet]
        public IActionResult CreateRole()
        {
            
            return View();
        }

        [HttpPost]
        public IActionResult CreateRole(RoleViewModel roleViewModel)
        {
            if (ModelState.IsValid)
            {
                AppRole role = new AppRole { Name = roleViewModel.Name };
                IdentityResult result = _roleManager.CreateAsync(role).Result;

                if (result.Succeeded)
                {
                    return RedirectToAction("Roles");
                }
                else
                {
                    AddModelError(result);
                }
            }

            return View(roleViewModel);
        }

        
        public IActionResult DeleteRole(string id)
        {
            if (id != null)
            {
                AppRole role = _roleManager.FindByIdAsync(id).Result;

                if (role != null)
                {
                    IdentityResult result = _roleManager.DeleteAsync(role).Result;
                }
            }

            return RedirectToAction("Roles");
        }

        [HttpGet]
        public IActionResult EditRole(string id)
        {
            AppRole role = _roleManager.FindByIdAsync(id).Result;
            RoleViewModel roleViewModel = role.Adapt<RoleViewModel>();
            return View(roleViewModel);
        }

        [HttpPost]
        public IActionResult EditRole(RoleViewModel roleViewModel)
        {
            
            if (ModelState.IsValid)
            {
                var role =_roleManager.FindByIdAsync(roleViewModel.Id).Result;
                role.Name = roleViewModel.Name;
                IdentityResult result = _roleManager.UpdateAsync(role).Result;

                if (result.Succeeded)
                {
                    ViewBag.success = "Melumatlar guncellendi";
                    return RedirectToAction("Roles");
                }
                else
                {
                    AddModelError(result);
                }
            }
            else
            {
                ModelState.AddModelError("","Melumatlari duzgun daxil edin");
            }

            return View(roleViewModel);
        }

        [HttpGet]
        public IActionResult RoleAssign(string id)
        {
            AppUser user = _userManager.FindByIdAsync(id).Result;

            TempData["userId"] = id;

            IQueryable<AppRole> roles = _roleManager.Roles;

            List<string> userRoles = _userManager.GetRolesAsync(user).Result as List<string>;

            List<RoleAssignViewModel> roleAssignViewModels = new List<RoleAssignViewModel>();

            foreach (var role in roles)
            {
                RoleAssignViewModel r = new RoleAssignViewModel();
                r.RoleId = role.Id;
                r.RoleName = role.Name;
                if (userRoles.Contains(role.Name))
                {
                    r.Exist = true;
                }
                else
                {
                    r.Exist = false;
                }

                roleAssignViewModels.Add(r);
            }

            return View(roleAssignViewModels);
        }

        [HttpPost]
        public async Task<IActionResult> RoleAssign(List<RoleAssignViewModel> roleAssignViewModels)
        {
            string userId = TempData["userId"].ToString();

            AppUser user = await _userManager.FindByIdAsync(userId);

            foreach (var role in roleAssignViewModels)
            {
                if (role.Exist)
                {
                    await _userManager.AddToRoleAsync(user,role.RoleName);
                }
                else
                {
                    await _userManager.RemoveFromRoleAsync(user,role.RoleName);
                }   
            }

            return RedirectToAction("Users");
        }
    }
}
