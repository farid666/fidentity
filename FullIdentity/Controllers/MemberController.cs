﻿using FullIdentity.Enums;
using FullIdentity.Models;
using Mapster;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace FullIdentity.Controllers
{
    [Authorize]
    public class MemberController : BaseController
    {

        public MemberController(UserManager<AppUser> userManager,SignInManager<AppUser> signInManager):base(userManager,signInManager)
        {
            
        }


        public IActionResult Index()
        {
            AppUser user = CurrentUser;
            MemberViewModel memberViewModel = user.Adapt<MemberViewModel>();
            return View(memberViewModel);
        }

        [HttpGet]
        public IActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        public IActionResult ChangePassword(ChangePasswordViewModel changePasswordViewModel)
        {
            AppUser user = CurrentUser;

            if (changePasswordViewModel.OldPassword != null)
            {
                bool exist = _userManager.CheckPasswordAsync(user, changePasswordViewModel.OldPassword).Result;

                if (exist)
                {

                    if (changePasswordViewModel.NewPassword == changePasswordViewModel.NewPasswordConfirm)
                    {
                        IdentityResult result = _userManager.ChangePasswordAsync(user, changePasswordViewModel.OldPassword, changePasswordViewModel.NewPassword).Result;

                        if (result.Succeeded)
                        {
                            _userManager.UpdateSecurityStampAsync(user);

                            /*_signInManager.SignOutAsync();
                            _signInManager.PasswordSignInAsync(user,changePasswordViewModel.NewPassword,true,false);*/ //bulari versek parolu deyisdikden sonra 30 dqhye bizi logine atmaz ozu cixis edib giris edecek

                            ViewBag.success = "true";
                        }
                        else
                        {
                            AddModelError(result);
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("", "Sifreler eyni deyil");
                    }

                }
                else
                {
                    ModelState.AddModelError("", "Kohne sifreniz duzgun deyil");
                }
            }
            else
            {
                ModelState.AddModelError("","Fieldlar bos ola bilmez");
            }
            
            return View(changePasswordViewModel);
        }

        [HttpGet]
        public IActionResult EditUser()
        {
            AppUser user = CurrentUser;
            MemberViewModel editUserViewModel = user.Adapt<MemberViewModel>();
            ViewBag.Gender = new SelectList(Enum.GetNames(typeof(Gender)));
            return View(editUserViewModel);
        }

        [HttpPost]
        public async Task<IActionResult> EditUser(MemberViewModel editUserViewModel,IFormFile userPicture)
        {

            ViewBag.Gender = new SelectList(Enum.GetNames(typeof(Gender)));

            if (ModelState.IsValid)
            {
                AppUser user = CurrentUser;

                if (userPicture != null && userPicture.Length > 0)
                {
                    var fileName = Guid.NewGuid() + Path.GetExtension(userPicture.FileName);

                    var path = Path.Combine(Directory.GetCurrentDirectory(),"wwwroot/userImage",fileName);

                    using (var stream  = new FileStream(path,FileMode.Create))
                    {
                        await userPicture.CopyToAsync(stream);

                        user.Picture = "/userImage/" + fileName;
                    }
                }

                user.UserName = editUserViewModel.UserName;
                user.Email = editUserViewModel.Email;
                user.PhoneNumber = editUserViewModel.PhoneNumber;
                user.City = editUserViewModel.City;
                user.BirthDate = editUserViewModel.BirthDate;
                user.Gender = (int)editUserViewModel.Gender;

                IdentityResult result = await _userManager.UpdateAsync(user);

                if (result.Succeeded)
                {
                    await _userManager.UpdateSecurityStampAsync(user);

                    ViewBag.success = "true";
                }
                else
                {
                    AddModelError(result);
                }
            }
            
            return View(editUserViewModel);
        }

        public IActionResult LogOut()
        {
            _signInManager.SignOutAsync();
            return RedirectToAction("Index","Home");
        }

        [Authorize(Roles = "Editor,Admin")]
        public IActionResult Editor()
        {
            return View();
        }

        [Authorize(Roles = "Manager,Admin")]
        public IActionResult Manager()
        {
            return View();
        }

        public IActionResult MyClaims()
        {
            
            return View(User.Claims.ToList());
        }

        [Authorize(Policy = "BakuPolicy")]
        public IActionResult Baku()
        {
            return View();
        }

        [Authorize(Policy = "ViolencePolicy")]
        public IActionResult Violence()
        {
            return View();
        }


        public async Task<IActionResult> ExchangeRedirect()
        {
            bool result = User.HasClaim(x => x.Type == "ExpireDateExchange");

            if (!result)
            {
                Claim claim = new Claim("ExpireDateExchange",DateTime.Now.AddDays(30).Date.ToShortDateString(),ClaimValueTypes.String,"Internal");

                await _userManager.AddClaimAsync(CurrentUser,claim);
                await _signInManager.SignOutAsync();
                await _signInManager.SignInAsync(CurrentUser,true);
            }

            return RedirectToAction("Exchange");
        }

        [Authorize(Policy = "ExchangePolicy")]
        public IActionResult Exchange()
        {
            return View();
        }
    }
}
