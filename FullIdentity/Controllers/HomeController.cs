﻿using FullIdentity.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace FullIdentity.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private UserManager<AppUser> _userManager;
        private SignInManager<AppUser> _signInManager;

        public HomeController(ILogger<HomeController> logger, UserManager<AppUser> userManager, SignInManager<AppUser> signInManager)
        {
            _logger = logger;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult SignUp()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> SignUp(SignUpViewModel signUpViewModel)
        {
            if (ModelState.IsValid)
            {
                AppUser user = new AppUser();
                user.UserName = signUpViewModel.UserName;
                user.Email = signUpViewModel.Email;
                user.PhoneNumber = signUpViewModel.PhoneNumber;

                IdentityResult result = await _userManager.CreateAsync(user, signUpViewModel.Password);

                if (result.Succeeded)
                {
                    string emailConfirmTOken = await _userManager.GenerateEmailConfirmationTokenAsync(user);

                    string confirmLink = Url.Action("ConfirmEmail","Home", new { userId = user.Id, token = emailConfirmTOken },HttpContext.Request.Scheme);

                    Helper.EmailConfirmation.SendEmail(confirmLink,user.Email);

                    return RedirectToAction("Login");
                }
                else
                {
                    foreach (var item in result.Errors)
                    {
                        ModelState.AddModelError("", item.Description);
                    }
                }
            }
            return View(signUpViewModel);
        }

        [HttpGet]
        public IActionResult Login(string ReturnUrl)
        {
            TempData["ReturnUrl"] = ReturnUrl;//burda Return Url aliriq login postda success olanda yonlendiririk
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel loginViewModel)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(loginViewModel.Email);

                if (user != null)
                {
                    if (await _userManager.IsLockedOutAsync(user))
                    {
                        ModelState.AddModelError("", "Profiliniz Kilitlenib Zehmet olmasa sonra bir daha yoxlayin");
                        return View(loginViewModel);
                    }

                    if (!await _userManager.IsEmailConfirmedAsync(user))
                    {
                        ModelState.AddModelError("", "Email adresinizi tesdiqleyin");
                        return View(loginViewModel);
                    }

                    await _signInManager.SignOutAsync();

                    Microsoft.AspNetCore.Identity.SignInResult result = await _signInManager.PasswordSignInAsync(user, loginViewModel.Password, loginViewModel.RememberMe, true);

                    if (result.Succeeded)
                    {

                        await _userManager.ResetAccessFailedCountAsync(user);

                        if (TempData["ReturnUrl"] != null) //bu login olmamisdan getdiyimiz sehife varsa ora yonlendirir
                        {
                            return Redirect(TempData["ReturnUrl"].ToString());
                        }


                        return RedirectToAction("Index", "Member");
                    }
                    else
                    {

                        int fail = await _userManager.GetAccessFailedCountAsync(user);//bu hem sehv giris sayini gosderir hemde artirir

                        ModelState.AddModelError("", $"Sehv giris sayiniz {fail}");

                        if (fail == 3)
                        {
                            await _userManager.SetLockoutEndDateAsync(user, new DateTimeOffset(DateTime.Now.AddMinutes(20))); // 20 dqlik lock edirik hesabi
                            ModelState.AddModelError("", "3 defe sehv giris etdiyiniz ucun hesabiniz 20 dqlik kilitlendi");
                        }
                        else
                        {
                            ModelState.AddModelError("", "Email ve ya Sifre sehvdir");
                        }

                    }
                }
                else
                {
                    ModelState.AddModelError("", "Bele isdifadeci tapilmadi");
                }
            }
            return View(loginViewModel);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpGet]
        public IActionResult ResetPassword()
        {
            return View();
        }

        [HttpPost]
        public IActionResult ResetPassword(ResetPasswordViewModel resetPasswordViewModel)
        {
            AppUser user = _userManager.FindByEmailAsync(resetPasswordViewModel.Email).Result;

            if (user != null)
            {
                string resetPassToken = _userManager.GeneratePasswordResetTokenAsync(user).Result;

                string resetPasswordLink = Url.Action("ResetPasswordConfirm", "Home", new
                {
                    UserId = user.Id,
                    token = resetPassToken
                }, HttpContext.Request.Scheme);

                Helper.ResetPassword.ResetPasswordSendEmail(resetPasswordLink, resetPasswordViewModel.Email);

                ViewBag.status = "successfully";

            }
            else
            {
                ModelState.AddModelError("", "Bele bir istifadeci tapilmadi");
            }
            return View(resetPasswordViewModel);
        }

        [HttpGet]
        public IActionResult ResetPasswordConfirm(string userId, string token)
        {
            TempData["userId"] = userId;
            TempData["token"] = token;

            return View();
        }


        [HttpPost]
        public async Task<IActionResult> ResetPasswordConfirm([Bind("NewPassword")] ResetPasswordViewModel resetPasswordViewModel)//Bind field adi yeni ancaq bunu doldur ver
        {
            string userId = TempData["userId"].ToString();
            var token = TempData["token"].ToString();


            AppUser user = await _userManager.FindByIdAsync(userId);

            if (user != null)
            {
                IdentityResult result = await _userManager.ResetPasswordAsync(user, token, resetPasswordViewModel.NewPassword);

                if (result.Succeeded)
                {
                    await _userManager.UpdateSecurityStampAsync(user);
                    //securitstamp ona gore yeniliyirikki parol username kimi esas weylrde deyiwiklik olunsa ozunde eks etdirsin mobilde parolu deyissek
                    //securitystampi yenilemesek webde kohne parolnan kohne securitystampnangiris ede bilecek yenileyirikki her yere tesir etsin
                    ViewBag.success = "success";
                }
                else
                {
                    foreach (var item in result.Errors)
                    {
                        ModelState.AddModelError("", item.Description);
                    }
                }
            }
            else
            {
                ModelState.AddModelError("", "Xeta bas verdi daha sonra yeniden cehd edin");
            }


            return View(resetPasswordViewModel);
        }

        public IActionResult AccessDenied(string ReturnUrl)
        {
            if (ReturnUrl.Contains("Violence"))
            {
                ViewBag.message = "18 yasiniz tamam olmadigi ucun bu sehifeye giris qadagandir SHiddet icerir";
            }
            else if (ReturnUrl.Contains("Baku"))
            {
                ViewBag.message = "Bu sehifeye ancaq seher qeydiyyati Baki olanlar gire biler";
            }
            else if (ReturnUrl.Contains("Exchange"))
            {
                ViewBag.message = "Sizin 30 gunluk pulsuz sehuifenden isdifade imkaniniz bitmisdir zehmet olmasa odenis edin";
            }
            else
            {
                ViewBag.message = "Giris qadagandir";
            }
            return View();
        }

        public async Task<IActionResult> ConfirmEmail(string userId,string token)
        {
            AppUser user = await _userManager.FindByIdAsync(userId);

            if (user != null)
            {
                IdentityResult result = await _userManager.ConfirmEmailAsync(user,token);

                if (result.Succeeded)
                {
                    ViewBag.message = "Emailiniz tesdiqlendi";
                }
                else
                {
                    ViewBag.message = "Xeta bas verdi!";
                }
            }
            return View();
        }


        public IActionResult FacebookLogin(string ReturnUrl)
        {

            string redirectUrl = Url.Action("ExternalResponse","Home",new {ReturnUrl = ReturnUrl });//facebookda is bitennen sora bura qayitsin

            var properties = _signInManager.ConfigureExternalAuthenticationProperties("Facebook",redirectUrl);//facebookdan giris ucun olan property lerdi

            return new ChallengeResult("Facebook",properties);// challenge result icinde ne versek ora yonlendirir bu propertylernen
        }

        public IActionResult GoogleLogin(string ReturnUrl)
        {

            string redirectUrl = Url.Action("ExternalResponse", "Home", new { ReturnUrl = ReturnUrl });//facebookda is bitennen sora bura qayitsin

            var properties = _signInManager.ConfigureExternalAuthenticationProperties("Google", redirectUrl);//facebookdan giris ucun olan property lerdi

            return new ChallengeResult("Google", properties);// challenge result icinde ne versek ora yonlendirir bu propertylernen
        }

     /*   public IActionResult MicrosoftLogin(string ReturnUrl)
        {

            string redirectUrl = Url.Action("ExternalResponse", "Home", new { ReturnUrl = ReturnUrl });//facebookda is bitennen sora bura qayitsin

            var properties = _signInManager.ConfigureExternalAuthenticationProperties("Microsoft", redirectUrl);//facebookdan giris ucun olan property lerdi

            return new ChallengeResult("Microsoft", properties);// challenge result icinde ne versek ora yonlendirir bu propertylernen
        }*/

        public async Task<IActionResult> ExternalResponse(string ReturnUrl = "/member")
        {
            ExternalLoginInfo info = await _signInManager.GetExternalLoginInfoAsync();//bu facebookda login oldugumuzla elaqeder infolari verir provideri ve userId facebookdaki

            if (info == null)
            {
                return RedirectToAction("Login");
            }
            else
            {
                Microsoft.AspNetCore.Identity.SignInResult result = await _signInManager.ExternalLoginSignInAsync(info.LoginProvider,info.ProviderKey,true);

                if (result.Succeeded)//succeeded dise demdeli bizim bazada melumatlar var onda return urleye qaytara bilerik
                {
                    return Redirect(ReturnUrl);
                }
                else//ilk defe facebook login buttonuna basirsa else dusecek
                {
                    AppUser user = new AppUser();
                    user.Email = info.Principal.FindFirst(ClaimTypes.Email).Value;//bu facebookdan gelen emaili alir claimlerden
                    string externalUserId = info.Principal.FindFirst(ClaimTypes.NameIdentifier).Value;//facebookdaki userId alir

                    if (info.Principal.HasClaim(x => x.Type ==ClaimTypes.Name))
                    {
                        string userName = info.Principal.FindFirst(ClaimTypes.Name).Value;

                        userName = userName.Replace(" ", "-").ToLower() + externalUserId.Substring(0,5); //bunu ona gore edirikki save edende bazaya usernameler eyni olub error vermesin

                        user.UserName = userName;
                    }
                    else
                    {
                        user.UserName = info.Principal.FindFirst(ClaimTypes.Email).Value;//eger yuxaridaki claimde name olmasa gelib username email yazsin ferqli olsun deye
                    }

                    AppUser user2 = await _userManager.FindByEmailAsync(user.Email);//bunu ona gore yoxluyuruqki evvelce bazada bu mail varsa ferqli sos.sebekedem girende eyni mailnen login ede bilsin

                    if (user2 == null)
                    {
                        IdentityResult createResult = await _userManager.CreateAsync(user);

                        // bura qeder olan hisse melumatlari dbye save etmekdi login asagida olacaq

                        if (createResult.Succeeded)
                        {
                            IdentityResult loginResult = await _userManager.AddLoginAsync(user, info);//buda userlogin provideri userId leri save edir

                            if (loginResult.Succeeded)
                            {
                                //await _signInManager.SignInAsync(user,true); buda duzdu ama claime dusmur hardan geldiyi dogrusu asagidaki kimidi
                                await _signInManager.ExternalLoginSignInAsync(info.LoginProvider, info.ProviderKey, true);
                                return Redirect(ReturnUrl);
                            }
                        }
                    }
                    else
                    {
                        IdentityResult loginResult = await _userManager.AddLoginAsync(user2, info);

                        await _signInManager.ExternalLoginSignInAsync(info.LoginProvider,info.ProviderKey,true);

                        return Redirect(ReturnUrl);
                    }

                }
            }

            return RedirectToAction("Error");

        }
    }
}
