﻿using FullIdentity.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FullIdentity.Models
{
    public class EditUserViewModel
    {
        [Required(ErrorMessage = "Isdifadeci adi gereklidir")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Email gereklidir")]
        [EmailAddress(ErrorMessage = "Email formati duzgun deyil")]
        public string Email { get; set; }
        public string PhoneNumber { get; set; }


        public string City { get; set; }
        public string Picture { get; set; }
        public Gender Gender { get; set; }
        public DateTime? BirthDate { get; set; }
    }
}
