﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FullIdentity.Models
{
    public class SignUpViewModel
    {
        [Required(ErrorMessage = "Isdifadeci adi gereklidir")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Email gereklidir")]
        [EmailAddress(ErrorMessage = "Email formati duzgun deyil")]
        public string Email { get; set; }

        //[RegularExpression("")] eger isdesek regex nen format vere bilerik burda
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "Sifre gereklidir")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

    }
}
