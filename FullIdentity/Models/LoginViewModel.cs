﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FullIdentity.Models
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Email vacibdir")]
        [EmailAddress(ErrorMessage = "Email formati duzgun deyil")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Sifre vacibdir")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public bool RememberMe { get; set; }


    }
}
