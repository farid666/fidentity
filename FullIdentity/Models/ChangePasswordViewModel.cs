﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FullIdentity.Models
{
    public class ChangePasswordViewModel
    {
        [Required(ErrorMessage = "Kohne Sifre Vacibdir")]
        [DataType(DataType.Password)]
        public string OldPassword { get; set; }

        [Required(ErrorMessage = "Yeni Sifre Vacibdir")]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }

        [Required(ErrorMessage = "Tekrar Sifre Vacibdir")]
        [DataType(DataType.Password)]
        public string NewPasswordConfirm { get; set; }
    }
}
