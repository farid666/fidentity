﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FullIdentity.Models
{
    public class ResetPasswordViewModel
    {
        [Required(ErrorMessage = "Email vaibdir")]
        [EmailAddress(ErrorMessage = "Email formati duzgun deyil")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Sifre Vacibdir")]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }


    }
}
