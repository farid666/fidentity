﻿using FullIdentity.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace FullIdentity.ClaimProvider
{
    public class ClaimProvider : IClaimsTransformation
    {
        private UserManager<AppUser> _userManager;

        public ClaimProvider(UserManager<AppUser> userManager)
        {
            _userManager = userManager;
        }

        //dinamik olaraq claim elave etmek yeni bazaya save etmeden
        //burda claim qeyd etmekle sonra biz policy vere biliriikki city si baki olanlar ancaq Baku sehifesine daxil ola bilsin


        public async Task<ClaimsPrincipal> TransformAsync(ClaimsPrincipal principal) 
        {
            if (principal != null && principal.Identity.IsAuthenticated)
            {
                ClaimsIdentity identity = principal.Identity as ClaimsIdentity; //bunu sora claimi add elemek ucun aliriq

                AppUser user = await _userManager.FindByNameAsync(identity.Name);
                

                if (user != null)
                {
                    if (user.City != null)
                    {
                        if (!principal.HasClaim(c => c.Type == "city"))
                        {
                            Claim claim = new Claim("city",user.City,ClaimValueTypes.String,"Internal");

                            identity.AddClaim(claim);
                        }
                    }


                    if (user.BirthDate != null)
                    {
                        if (!principal.HasClaim(c => c.Type == "birthdate"))
                        {
                            var today = DateTime.Today;
                            var age = today.Year - user.BirthDate?.Year;

                            if (age > 18)
                            {
                                Claim claim = new Claim("birthdate", true.ToString(), ClaimValueTypes.String, "Internal");

                                identity.AddClaim(claim);
                            }

                        }
                    }
                }
            }

            return principal;
        }
    }
}
