﻿using FullIdentity.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FullIdentity.CustomValidate
{
    public class CustomPasswordValidators : IPasswordValidator<AppUser>
    {
        public Task<IdentityResult> ValidateAsync(UserManager<AppUser> manager, AppUser user, string password)
        {
            List<IdentityError> errors = new List<IdentityError>();

            if (password.ToLower().Contains(user.UserName.ToLower()))
            {
                errors.Add(new IdentityError { Code = "PasswordContainUsernam", Description = "isdifadeci adi sifrede qeyd oluna bilmez" });
            }


            if (password.ToLower().Contains(user.Email.ToLower()))
            {
                errors.Add(new IdentityError {Code="PasswordContainsEmail",Description = "Sifrede Email isdifade oluna bilmez" });
            }

            if (password.ToLower().Contains("1234"))
            {
                errors.Add(new IdentityError {Code = "PasswordContains1234",Description ="Sifrede ardicil ededler isdifade oluna bilmez"});
            }

            if (errors.Count == 0)
            {
                return Task.FromResult(IdentityResult.Success);
            }
            else
            {
                return Task.FromResult(IdentityResult.Failed(errors.ToArray()));
            }
        }
    }
}
