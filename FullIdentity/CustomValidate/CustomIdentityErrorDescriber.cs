﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FullIdentity.CustomValidate
{
    public class CustomIdentityErrorDescriber:IdentityErrorDescriber
    {

        public override IdentityError DuplicateUserName(string userName)
        {
            return new IdentityError() { Code = "DuplicateUserName", Description = "Isdifadeci adi artiq isdifade olunub" };
        }

        public override IdentityError DuplicateEmail(string email)
        {
            return new IdentityError() { Code = "DuplicateEmail", Description = "Email artiq isdifade olunub" };
        }

    }
}
